<?php

namespace MI;

class GEN
{

	public static function error($status = 0, $customMessage = null)
	{
		ob_get_clean();
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		http_response_code(400);
		echo json_encode(array('data' => null, 'message' => $customMessage, 'status' => intval($status)));
		exit;
	}

	public static function success($data, $status = 1, $message = null)
	{
		ob_get_clean();
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		http_response_code(200);
		echo json_encode(['data' => $data, 'message' => strval($message), 'status' => $status]);
		exit;
	}

	public static function redirect($url, $statusCode = 303)
	{
		ob_get_clean();
		header('Location: ' . $url, true, $statusCode);
		die();
	}

	/**
	 * @param $url
	 * @param $params
	 * @param string $method
	 * @return mixed
	 */
	public static function getResponse($url, $params, $method = "get")
	{
		$options = array(
			CURLOPT_RETURNTRANSFER => true,   // return web page
			CURLOPT_HEADER => false,  // don't return headers
			CURLOPT_FOLLOWLOCATION => true,   // follow redirects
			CURLOPT_AUTOREFERER => true,   // set referrer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
			CURLOPT_TIMEOUT => 120    // time-out on response
		);

		$ch = curl_init();
		curl_setopt_array($ch, $options);

		if (strtolower($method) == "post") {
			curl_setopt($ch, CURLOPT_POST, count($params));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		} else {
			curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($params));
		}

		$content = curl_exec($ch);

		curl_close($ch);

		return $content;
	}

	public static function isValid($password)
	{
		//If no alphabet characters found, return false.
		if (!preg_match("/[a-z]/i", $password)) {
			return false;
		}
		//If no digits found, return false.
		if (!preg_match("/[0-9]/", $password)) {
			return false;
		}
		//If no digits found, return false.
		$specialRegex = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
		if (!preg_match($specialRegex, $password)) {
			return false;
		}
		//If a space is found, return false.
		if (strpos($password, " ") !== false) {
			return false;
		}
		if (strlen($password) < 8) {
			return false;
		}
		//If nothing happened so far, the password is valid. Return true.
		return true;
	}

	public static function getDataURI($file)
	{
		$fInfo = finfo_open();
		$mime = finfo_file($fInfo, $file, FILEINFO_MIME_TYPE);
		finfo_close($fInfo);
		$contents = file_get_contents($file);
		$base64 = base64_encode($contents);
		$dataURI = 'data:' . $mime . ';base64,' . $base64;
		return $dataURI;
	}

}