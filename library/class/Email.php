<?php

class Email
{
	public function send($to_email, $to_name, $subject, $message)
	{
		global $EMAIL_USER, $EMAIL_PASS, $EMAIL_NAME, $EMAIL_REPLY_TO, $EMAIL_REPLY_NAME, $EMAIL_BCC_ID;
		$mail = new PHPMailer;
		if (substr($_SERVER['HTTP_HOST'], 0, 7) == "192.168" || $_SERVER['HTTP_HOST'] == "localhost") {
			//$mail->SMTPDebug = 3;
			$mail->isSMTP();
			$mail->Host = 'smtp.gmail.com';
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = 'tls';
			$mail->Port = 587;
		}
		$mail->Username = $EMAIL_USER;
		$mail->Password = $EMAIL_PASS;
		$mail->setFrom($EMAIL_USER, $EMAIL_NAME);
		$mail->addAddress($to_email, $to_name);     // Add a recipient
		$mail->addReplyTo($EMAIL_REPLY_TO, $EMAIL_REPLY_NAME);
		$mail->isHTML(true);                                  // Set email format to HTML
		#$mail->addBCC($EMAIL_BCC_ID, $EMAIL_REPLY_NAME);
		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->send();
	}

	public function sign_up($sender_email, $sender_name, $user_id)
	{
		global $site_url, $site_path, $upload_image_url, $SIGN_UP_SUBJECT;
		$html = file_get_contents($site_path . 'email_template/signup.html');
		$LINK = $site_url . 'verifyuser.php?id=' . base64_encode($user_id);
		## Replace Data
		$html = str_replace('##IMAGE##', $upload_image_url . 'logo.png', $html);
		$html = str_replace('##SENDER_NAME##', $sender_name, $html);
		$html = str_replace('##LINK##', $LINK, $html);
		$this->send($sender_email, $sender_name, $SIGN_UP_SUBJECT, $html);
	}

	public function forgot_password($sender_email, $sender_name, $code)
	{
		global $site_url, $site_path, $upload_image_url, $FORGOT_PASSWORD_SUBJECT;
		$html = file_get_contents($site_path . 'email_template/forgot_password.html');
		## Replace Data
		$html = str_replace('##IMAGE##', $upload_image_url . 'logo.png', $html);
		$html = str_replace('##SENDER_NAME##', $sender_name, $html);
		$html = str_replace('##CODE##', $code, $html);
		$this->send($sender_email, $sender_name, $FORGOT_PASSWORD_SUBJECT, $html);
	}

	public function forgot_pin($sender_email, $sender_name, $code)
	{
		global $site_url, $site_path, $upload_image_url, $FORGOT_PIN_SUBJECT;
		$html = file_get_contents($site_path . 'email_template/forgot_password.html');
		## Replace Data
		$html = str_replace('##IMAGE##', $upload_image_url . 'logo.png', $html);
		$html = str_replace('##SENDER_NAME##', $sender_name, $html);
		$html = str_replace('##CODE##', $code, $html);
		$this->send($sender_email, $sender_name, $FORGOT_PIN_SUBJECT, $html);
	}
}