<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    user_login_devices
 * DATE:         12.07.2016
 * CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/user_login_devices.class.php
 * TABLE:        user_login_devices
 * DB:           flexpay
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class user_login_devices
{


	/**
	 * @desc Variable Declaration with default value
	 */

	protected $id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_id;
	protected $_user_id;
	protected $_device_id;
	protected $_player_id;
	protected $_added_at;


	/**
	 * @desc   CONSTRUCTOR METHOD
	 */

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_id = null;
		$this->_user_id = null;
		$this->_device_id = null;
		$this->_player_id = null;
		$this->_added_at = null;
	}

	/**
	 * @desc   DECONSTRUCTOR METHOD
	 */

	function __destruct()
	{
		unset($this->_obj);
	}


	/**
	 * @desc   GETTER METHODS
	 */

	public function getid()
	{
		return $this->_id;
	}

	public function getuser_id()
	{
		return $this->_user_id;
	}

	public function getdevice_id()
	{
		return $this->_device_id;
	}

	public function getplayer_id()
	{
		return $this->_player_id;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	/**
	 * @desc   SETTER METHODS
	 */

	public function setid($val)
	{
		$this->_id = $val;
	}

	public function setuser_id($val)
	{
		$this->_user_id = $val;
	}

	public function setdevice_id($val)
	{
		$this->_device_id = $val;
	}

	public function setplayer_id($val)
	{
		$this->_player_id = $val;
	}

	public function setadded_at($val)
	{
		$this->_added_at = $val;
	}

	/**
	 * @desc   SELECT METHOD / LOAD
	 */

	function select($id = null)
	{
		$WHERE = 'WHERE 1=1';
		if (!empty($id)) {
			$WHERE .= ' AND id = ' . $id;
		}
		$sql = "SELECT * FROM user_login_devices $WHERE order by id";
		$row = $this->_obj->select($sql);

		$this->_id = $row[0]['id'];
		$this->_user_id = $row[0]['user_id'];
		$this->_device_id = $row[0]['device_id'];
		$this->_player_id = $row[0]['player_id'];
		$this->_added_at = $row[0]['added_at'];
		return $row;
	}

	/**
	 * @desc   DELETE
	 */

	function delete($id)
	{
		$sql = "DELETE FROM user_login_devices WHERE id = $id";
		$this->_obj->sql_query($sql);
	}

	/**
	 * @desc   INSERT
	 */

	function insert()
	{
		$this->id = ""; // clear key for autoincrement

		$sql = "INSERT INTO user_login_devices ( device_id,player_id,user_id,added_at ) VALUES ( '" . $this->_device_id . "','" . $this->_player_id . "','" . $this->_user_id . "','" . $this->_added_at . "' )";
		$result = $this->_obj->insert($sql);
		return $result;
	}

	/**
	 * @desc   UPDATE
	 */

	function update($id)
	{
		$sql = "UPDATE user_login_devices SET device_id = '" . $this->_device_id . "' , player_id = '" . $this->_player_id . "' , user_id = '" . $this->_user_id . "' , added_at = '" . $this->_added_at . "' WHERE id = $id ";
		$this->_obj->sql_query($sql);
	}

	/**
	 * @desc   remove subuser from all devices
	 */

	function removeSubUser($id)
	{
		$sql = "DELETE FROM user_login_devices WHERE user_id = '{$id}'";
		$this->_obj->sql_query($sql);
		return;
	}

	/**
	 * @desc check sub user login on a device.
	 */

	function checkSubUserDeviceLogin($id, $device_id)
	{
		$sql = "SELECT COUNT(*) as tot FROM user_login_devices WHERE device_id = '{$device_id}' AND user_id = '{$id}'";
		$result = $this->_obj->select($sql);
		return ($result[0]['tot'] > 0) ? true : false;
	}

	/**
	 * @desc check User device & player registered
	 */

	function registerUserDevice($id, $player_id, $user_id)
	{
		$sql = "Update user_login_devices SET player_id = '{$player_id}', user_id = '{$user_id}' WHERE id = '{$id}'";
		$this->_obj->sql_query($sql);
	}

	/**
	 * @desc reset user from all devices
	 */

	function resetUserDevice($user_id, $device_id, $player_id = "")
	{
		$wsql = "";

		if ($player_id != "") {
			$wsql = " AND player_id = '{$player_id}' ";
		}
		$sql = "DELETE FROM user_login_devices WHERE user_id = '{$user_id}' AND device_id = '{$device_id}' {$wsql}";
		$this->_obj->sql_query($sql);
		return;
	}

	/**
	 * @desc check user with their device
	 */

	function checkUserDevice($device_id, $user_id = "", $player_id = "")
	{
		$psql = "";
		$usql = "";

		if ($player_id != "") {
			$psql = " AND player_id = '{$player_id}' ";
		}

		if ($user_id != "") {
			$usql = " AND user_id = '{$user_id}' ";
		}
		$sql = "SELECT COUNT(*) as tot, id FROM user_login_devices WHERE device_id = '{$device_id}' {$psql} {$usql}";
		$result = $this->_obj->select($sql);

		if ($result[0]['tot'] > 0 && ($result[0]['id'] != "" || $result[0]['id'] != 0)) {
			return $result[0]['id'];
		} else {
			return 0;
		}
	}

}