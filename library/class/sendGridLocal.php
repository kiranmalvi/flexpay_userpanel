<?php

class SendGridLocal extends \SendGrid\Email
{
	##################################################################################
	## Don't Touch Code
	protected $SendGrid;
	protected $sendGridObj;

	public function __construct()
	{
		$SendGrid = new SendGrid('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');
		$sendGridObj = new SendGrid\Email();
		$this->SendGrid = $SendGrid;
		$this->sendGridObj = $sendGridObj;
	}

	private function sendSendGrid($email, $name, $subject)
	{
		global $EMAIL_USER, $EMAIL_NAME;
		$this->sendGridObj
			->addTo($email)
			->addToName($name)
			->setFrom($EMAIL_USER)
			->setFromName($EMAIL_NAME)
			->setSubject($subject);

		return $this->SendGrid->send($this->sendGridObj);
	}
	##################################################################################

	/// Start
	public function signup($to_mail, $name, $link)
	{
		global $EMAIL_NAME;
		$sign_up_template_id = "e0838e4e-64b2-4d94-96f5-fa0e3fc9cd23";

		$this->sendGridObj
			->setHtml($EMAIL_NAME)
			->addFilter('templates', 'enabled', 1)
			->addFilter('templates', 'template_id', $sign_up_template_id)
			->addSubstitution(':name', array($name))
			->addSubstitution(':projectname', array($EMAIL_NAME))
			->addSubstitution(':verification', array($link));

		$status = $this->sendSendGrid($to_mail, $name, 'Welcome to Flexpay');
		return $status;
	}

	public function forgotpassword($to_mail, $name, $code)
	{
		global $EMAIL_NAME;
		$forgot_pass_template_id = "1b92b204-f455-46e9-8026-5f4c86fd91a0";
		$this->sendGridObj
			->setHtml($EMAIL_NAME)
			->addFilter('templates', 'enabled', 1)
			->addFilter('templates', 'template_id', $forgot_pass_template_id)
			->addSubstitution(':name', array($name))
			->addSubstitution(':code', array($code));

		$status = $this->sendSendGrid($to_mail, $name, 'Flexpay - forgot password');
		return $status;
	}

	public function forgotpin($to_mail, $name, $code)
	{
		global $EMAIL_NAME;
		$forgot_pin_template_id = "25e55f9c-c7e4-4bcf-bef2-a6631fbd2f9f";
		$this->sendGridObj
			->setHtml($EMAIL_NAME)
			->addFilter('templates', 'enabled', 1)
			->addFilter('templates', 'template_id', $forgot_pin_template_id)
			->addSubstitution(':name', array($name))
			->addSubstitution(':code', array($code));

		$status = $this->sendSendGrid($to_mail, $name, 'Flexpay - forgot pin');
		return $status;
	}

	public function vendor_email($to_mail, $name)
	{
		$return = $this->vendor_active_mail($to_mail, $name);
		$this->sendGridObj
			->setHtml('Pigi')
			->addFilter('templates', 'template_id', $return['template']);

		foreach ($return['substitution'] as $k => $v) {
			$this->sendGridObj->addSubstitution($k, $v);
		}
		$status = $this->sendSendGrid($to_mail, $name, 'Pigi User verification');
		return $status;
	}

	public function Invoice_send($to_mail, $name, $subject, $content, $attachment = null, $ex_name)
	{
		$this->sendGridObj
			->setHtml($content)
			->setAttachment($attachment, $ex_name . '.pdf');

		$status = $this->sendSendGrid($to_mail, '', $subject);
		return $status;
	}

	public function Invoice_send_customer($to_mail, $name, $subject, $content, $attachment = null, $ex_name)
	{
		$this->sendGridObj
			->setHtml($content)
			->setAttachment($attachment, $ex_name . '.pdf');

		$status = $this->sendSendGrid($to_mail, '', $subject);
		return $status;
	}
}