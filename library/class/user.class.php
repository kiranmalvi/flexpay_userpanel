<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    user
 * DATE:         28.10.2015
 * CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/user.class.php
 * TABLE:        user
 * DB:           flexpay
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class user
{


	/**
	 * @desc Variable Declaration with default value
	 */

	protected $id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_id;
	protected $_unique_token;
	protected $_employee_id;
	protected $_parent_user_id;
	protected $_description;
	protected $_name;
	protected $_email;
	protected $_password;
	protected $_mobile;
	protected $_security_pin;
	protected $_qr_code;
	protected $_verification_code;
	protected $_company_name;
	protected $_transfer_limit;
	protected $_notes;
	protected $_added_at;
	protected $_updated_at;
	protected $_status;


	/**
	 * @desc   CONSTRUCTOR METHOD
	 */

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_id = null;
		$this->_unique_token = null;
		$this->_employee_id = null;
		$this->_parent_user_id = null;
		$this->_description = null;
		$this->_name = null;
		$this->_email = null;
		$this->_password = null;
		$this->_mobile = null;
		$this->_security_pin = null;
		$this->_company_name = null;
		$this->_transfer_limit = null;
		$this->_notes = null;
		$this->_qr_code = null;
		$this->_verification_code = null;
		$this->_added_at = null;
		$this->_updated_at = null;
		$this->_status = null;
	}

	/**
	 * @desc   DECONSTRUCTOR METHOD
	 */

	function __destruct()
	{
		unset($this->_obj);
	}


	/**
	 * @desc   GETTER METHODS
	 */


	public function getid()
	{
		return $this->_id;
	}

	/**
	 * @desc   SETTER METHODS
	 */


	public function setid($val)
	{
		$this->_id = $val;
	}

	public function getemployee_id()
	{
		return $this->_employee_id;
	}

	public function setemployee_id($val)
	{
		$this->_employee_id = $val;
	}

	public function getparent_user_id()
	{
		return $this->_parent_user_id;
	}

	public function setparent_user_id($val)
	{
		$this->_parent_user_id = $val;
	}

	public function getdescription()
	{
		return $this->_description;
	}

	public function setdescription($val)
	{
		$this->_description = $val;
	}

	public function getname()
	{
		return $this->_name;
	}

	public function setname($val)
	{
		$this->_name = $val;
	}

	public function getemail()
	{
		return $this->_email;
	}

	public function setemail($val)
	{
		$this->_email = $val;
	}

	public function getpassword()
	{
		return $this->_password;
	}

	function get_password($password = null, $id = null)
	{
		$sql = "SELECT * FROM user where   (password ='$password' AND id IN ($id)) AND status !='2' order by id";
		$row = $this->_obj->select($sql);
		return $row;

	}

	public function setpassword($val)
	{
		$this->_password = $val;
	}

	public function getmobile()
	{
		return $this->_mobile;
	}

	public function setmobile($val)
	{
		$this->_mobile = $val;
	}

	public function getsecurity_pin()
	{
		return $this->_security_pin;
	}

	public function setsecurity_pin($val)
	{
		$this->_security_pin = $val;
	}

	public function getcompany_name()
	{
		return $this->_company_name;
	}

	public function setcompany_name($val)
	{
		$this->_company_name = $val;
	}

	public function gettransfer_limit()
	{
		return $this->_transfer_limit;
	}

	public function settransfer_limit($val)
	{
		$this->_transfer_limit = $val;
	}

	public function getnotes()
	{
		return $this->_notes;
	}

	public function setnotes($val)
	{
		$this->_notes = $val;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function setadded_at($val)
	{
		$this->_added_at = $val;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}

	public function setupdated_at($val)
	{
		$this->_updated_at = $val;
	}

	public function getstatus()
	{
		return $this->_status;
	}

	public function setstatus($val)
	{
		$this->_status = $val;
	}

	/**
	 * @desc   SELECT METHOD / LOAD
	 */

	function select($id = null)
	{
		$WHERE = 'WHERE 1=1';
		if (!empty($id)) {
			$WHERE .= ' AND id = ' . $id;
		}
		$sql = "SELECT * FROM user $WHERE AND status !='2' order by id";
		$row = $this->_obj->select($sql);

		$this->_id = $row[0]['id'];
		$this->_unique_token = $row[0]['unique_token'];
		$this->_employee_id = $row[0]['employee_id'];
		$this->_parent_user_id = $row[0]['parent_user_id'];
		$this->_description = $row[0]['description'];
		$this->_name = $row[0]['name'];
		$this->_email = $row[0]['email'];
		$this->_password = $row[0]['password'];
		$this->_mobile = $row[0]['mobile'];
		$this->_security_pin = $row[0]['security_pin'];
		$this->_company_name = $row[0]['company_name'];
		$this->_transfer_limit = $row[0]['transfer_limit'];
		$this->_notes = $row[0]['notes'];
		$this->_qr_code = $row[0]['qr_code'];
		$this->_verification_code = $row[0]['verification_code'];
		$this->_added_at = $row[0]['added_at'];
		$this->_updated_at = $row[0]['updated_at'];
		$this->_status = $row[0]['status'];
		return $row;
	}

	function check_user_email($email)
	{
		$sql = "SELECT count(id) as tot FROM user WHERE email = '{$email}'";
		$row = $this->_obj->select($sql);
		return ($row[0]['tot'] == 0) ? true : false;
	}

	function check_email($email)
	{
		$sql = "SELECT id,status,parent_user_id FROM user WHERE email = '{$email}'";
		$row = $this->_obj->select($sql);
		return $row;
	}

	function check_unique_token($token)
	{
		$sql = "SELECT count(id) as tot FROM user WHERE unique_token = '{$token}'";
		$row = $this->_obj->select($sql);
		return ($row[0]['tot'] == 0) ? true : false;
	}

	function check_user_exsits($user_id)
	{
		$sql = "SELECT count(id) as tot FROM user WHERE id = '{$user_id}'";
		$row = $this->_obj->select($sql);
		return ($row[0]['tot'] == 1) ? true : false;
	}

	/**
	 * @desc   DELETE
	 */

	function delete($id)
	{
		$sql = "DELETE FROM user WHERE id = $id";
		$this->_obj->sql_query($sql);
	}

	/**
	 * @desc   INSERT
	 */

	function insert()
	{
		$this->id = ""; // clear key for autoincrement

		$sql = "INSERT INTO user ( unique_token,employee_id,parent_user_id,description,name,email,password,mobile,security_pin,company_name,transfer_limit,notes,added_at,updated_at,status ) VALUES ( '" . $this->_unique_token . "','" . $this->_employee_id . "','" . $this->_parent_user_id . "','" . $this->_description . "','" . $this->_name . "','" . $this->_email . "','" . $this->_password . "','" . $this->_mobile . "','" . $this->_security_pin . "','" . $this->_company_name . "','" . $this->_transfer_limit . "','" . $this->_notes . "','" . $this->_added_at . "','" . $this->_updated_at . "','" . $this->_status . "' )";
		$result = $this->_obj->insert($sql);
		return $result;
	}

	/**
	 * @desc   UPDATE
	 */

	function update($id)
	{

		$sql = " UPDATE user SET  employee_id = '" . $this->_employee_id . "' , parent_user_id = '" . $this->_parent_user_id . "' , description = '" . $this->_description . "' , name = '" . $this->_name . "' , email = '" . $this->_email . "' , password = '" . $this->_password . "' , mobile = '" . $this->_mobile . "' , security_pin = '" . $this->_security_pin . "' , company_name = '" . $this->_company_name . "' , transfer_limit = '" . $this->_transfer_limit . "' , notes = '" . $this->_notes . "' , added_at = '" . $this->_added_at . "' , updated_at = '" . $this->_updated_at . "' , status = '" . $this->_status . "'  WHERE id = $id ";
		$this->_obj->sql_query($sql);

	}

	function profile_update($id, $name, $mobile, $company = null, $date = null)
	{

		$sql = " UPDATE user SET  name = '$name'  , mobile = '$mobile', company_name='$company'  , updated_at = '$date' , status = '1'  WHERE id = $id ";
		$this->_obj->sql_query($sql);

	}

	function update_qr_code($user_id, $name)
	{
		$sql = " UPDATE user SET qr_code = '{$name}'  WHERE id = $user_id ";
		$this->_obj->sql_query($sql);
	}

	function get_pin($pin = null, $id = null)
	{
		$sql = "SELECT * FROM user where   (security_pin ='$pin' AND id IN ($id)) AND status !='2' order by id";
		$row = $this->_obj->select($sql);
		return $row;

	}

	function get_login($password = null, $email = null)
	{
		$sql = "SELECT * FROM user where   (password ='$password' AND email='$email') AND status !='2'  ";
		$row = $this->_obj->select($sql);
		return $row;

	}

	function passwordupdate($id = null, $password = null)
	{
		$sql = " UPDATE user SET   password = '$password'    WHERE id = $id ";
		$result = $this->_obj->sql_query($sql);
		if ($result > 0) {
			return true;

		} else {
			return false;
		}
	}

	function updatepin($id = null, $pin = null)
	{
		$sql = " UPDATE user SET   security_pin = '$pin'    WHERE id = $id ";
		$result = $this->_obj->sql_query($sql);
		if ($result > 0) {
			return true;

		} else {
			return false;
		}
	}

	/**
	 * @return mixed
	 */
	public function getUniqueToken()
	{
		return $this->_unique_token;
	}

	/**
	 * @param mixed $unique_token
	 */
	public function setUniqueToken($unique_token)
	{
		$this->_unique_token = $unique_token;
	}

	/**
	 * @return mixed
	 */
	public function getQrCode()
	{
		return $this->_qr_code;
	}

	/**
	 * @param mixed $qr_code
	 */
	public function setQrCode($qr_code)
	{
		$this->_qr_code = $qr_code;
	}

	/**
	 * @return mixed
	 */
	public function getVerificationCode()
	{
		return $this->_verification_code;
	}

	/**
	 * @param mixed $verification_code
	 */
	public function setVerificationCode($verification_code)
	{
		$this->_verification_code = $verification_code;
	}

	function update_status($id)
	{

		$sql = " UPDATE user SET   status = '1'  WHERE id = $id ";
		$this->_obj->sql_query($sql);

	}

	function check_user($email)
	{
		$sql = "SELECT id, status,name,email,verification_code FROM user WHERE email = '{$email}'";
		$row = $this->_obj->select($sql);
		//pr($row);exit;
		return $row;
	}

	function update_code($code = '', $id, $gmt)
	{
//        $date = date('Y-m-d H:i:s');
		$sql = "UPDATE user SET verification_code = '{$code}', updated_at = '{$gmt}' WHERE id = '{$id}'";
		$row = $this->_obj->sql_query($sql);
		return $row;
	}

	function update_password($password, $id, $gmt)
	{
//        $date = date('Y-m-d H:i:s');
		$sql = "UPDATE user SET password = '{$password}', updated_at = '{$gmt}' WHERE id = '{$id}'";
		$row = $this->_obj->sql_query($sql);
		return $row;
	}

	function update_pin($pin, $id, $gmt)
	{
//        $date = date('Y-m-d H:i:s');
		$sql = "UPDATE user SET security_pin = '{$pin}', updated_at = '{$gmt}' WHERE id = '{$id}'";
		$row = $this->_obj->sql_query($sql);
		return $row;
	}

	function get_total_users()
	{
		$sql = "SELECT count(*) as tot FROM user WHERE status = '1' ";
		$row = $this->_obj->select($sql);
		//pr($row);exit;
		return $row;
	}

	function total_amount_in_user_wallet()
	{
		$sql = "SELECT sum(amount) as total_amt FROM wallet WHERE status = '1' ";
		$row = $this->_obj->select($sql);
		//pr($row);exit;
		return $row;
	}


	function total_trasactions()
	{
		$sql = "SELECT count(*) as total_transaction FROM transaction WHERE status = '1' AND transaction_status = 'confirm' ";
		$row = $this->_obj->select($sql);
		//pr($row);exit;
		return $row;
	}

	function get_default_debit()
	{
		$sql = "SELECT vValue FROM setting WHERE vDesc = 'DEFAULT_DEBIT_RATE' ";
		$return = $this->_obj->select($sql);
		return $return[0]['vValue'];
	}

	function get_default_credit()
	{
		$sql = "SELECT vValue FROM setting WHERE vDesc = 'DEFAULT_CREDIT_RATE' ";
		$return = $this->_obj->select($sql);
		return $return[0]['vValue'];
	}

	function get_default_spend_limit()
	{
		$sql = "SELECT vValue FROM setting WHERE vDesc = 'DEFAULT_SPEND_LIMIT' ";
		$return = $this->_obj->select($sql);
		return $return[0]['vValue'];
	}

	function check_user_email_with_status($email, $status)
	{
		$sql = "SELECT count(id) as tot,id FROM user WHERE email = '{$email}' AND status = '$status' ";
		$row = $this->_obj->select($sql);
		#pr($row);
		return ($row[0]['tot'] == 0) ? false : $row[0]['id'];
	}

	function update_transfer_limit($id, $amount, $gmt)
	{
		$sql = "UPDATE user SET transfer_limit = '{$amount}', updated_at = '{$gmt}' WHERE id = '{$id}' ";
		$row = $this->_obj->sql_query($sql);
		return $row;
	}

	function update_user_note($id, $note, $gmt)
	{
		$sql = "UPDATE user SET notes = '{$note}', updated_at = '{$gmt}' WHERE id = '{$id}' ";
		$row = $this->_obj->sql_query($sql);
		return $row;
	}

	function check_spending_limit($user_id, $gmt, $amount)
	{
		//get user spending limit
		$user_limit = "SELECT transfer_limit FROM user WHERE id = '{$user_id}'";
		$user_record = $this->_obj->select($user_limit);
		$limit = $user_record[0]['transfer_limit'];

		//get user today spend amount
		$user_spend = "SELECT SUM(amount) as todays_spend FROM transaction WHERE to_user_id = '{$user_id}' AND DATE_FORMAT(added_at, '%Y-%m-%d') = '{$gmt}' AND type = 'debit' AND transaction_status = 'confirm' AND status = '1' AND from_user_id != 0";
		$spend_record = $this->_obj->select($user_spend);
		$spend_limit = $spend_record[0]['todays_spend'];
		$spend_limit = $spend_limit ? $spend_limit : 0;

		$return = new stdClass();
		$return->transfer_limit = $limit;
		$return->todays_spend = $spend_limit;

		return $return;
	}

	public function do_login($email, $password)
	{
		$SQL = "SELECT * FROM user WHERE email = '{$email}' AND password = '{$password}'";
		$DATA = $this->_obj->select($SQL);

		if (count($DATA) == 1) {
			## Check Status
			if ($DATA[0]['status'] == '1') {
				## Set Session
				$this->set_session($DATA[0]);
				global $generalfuncobj;
				#$generalfuncobj->func_set_temp_sess_msg("Well Come" . " " . $DATA[0]['name'], "success");
				header('location:index.php?file=ad-dashboard');
			} else {
				return "Your account is not activated yet. please contact administrator";
			}
		} else {
			return "You are not authorized user and try again";
		}
	}

	private function set_session($data)
	{
		global $Project_Name;
		$session = array(
			'id' => $data['id'],
			'email' => $data['email'],
			'user_name' => $data['name'],
		);
		$_SESSION[$Project_Name]['ADMIN'] = $session;
	}

	private function set_members_session($data)
	{
		global $Project_Name_Members;
		$session = array(
			'id' => $data['id'],
			'email' => $data['email'],
			'user_name' => $data['name'],
		);
		$_SESSION[$Project_Name_Members]['MEMBERS'] = $session;
	}

	public function members_do_login($email, $password)
	{
		$SQL = "SELECT * FROM user WHERE email = '{$email}' AND password = '{$password}' AND id != '0'";
		$DATA = $this->_obj->select($SQL);

		if (count($DATA) == 1) {
			## Check Status
			if ($DATA[0]['status'] == '1') {
				## Set Session
				$this->set_members_session($DATA[0]);
				global $generalfuncobj;
				$generalfuncobj->func_set_temp_sess_msg("Well Come" . " " . $DATA[0]['name'], "success");
				header('location:index.php?file=a-dashboard');
			} elseif ($DATA[0]['status'] == '0') {
				$_SESSION['tmp_message1'] = 'inactive';
			} elseif ($DATA[0]['status'] == '2') {
				$_SESSION['tmp_message2'] = 'deleted';
			} elseif ($DATA[0]['status'] == '3') {
				$_SESSION['tmp_message3'] = 'block';
			} elseif ($DATA[0]['status'] == '4') {
				$_SESSION['tmp_message4'] = 'unverified';
			} else {
				return "Your account is not activated yet. please contact administrator";
			}
		} else {
			$_SESSION['tmp_message'] = 'success';
			//return "You are not authorized user and try again";
		}
	}

	function checkoldpassword_user($checkpassword, $id)
	{

		$sql = "SELECT * from user where password='$checkpassword' AND id='$id'";
		$row = $this->_obj->select($sql);
		return $row;
	}

	function update_password_user($newpassword, $id)
	{

		$sql = " UPDATE user SET password = '$newpassword' WHERE id = $id ";
		$this->_obj->sql_query($sql);

	}

	function check_user_employeeid($employee_id, $id = '')
	{
		$wsql = ' WHERE 1=1 ';
		if ($id != '') {
			$wsql .= " AND id != '{$id}' ";
		}
		$sql = "SELECT count(id) as tot FROM user {$wsql} AND employee_id = '{$employee_id}' and status = '1' ";
		$row = $this->_obj->select($sql);
		return ($row[0]['tot'] == 0) ? true : false;
	}

	function profile_update_subUser($userid, $id, $name, $employee_id, $desc = null, $date = null)
	{

		$sql = " UPDATE user SET  name = '{$name}'  , employee_id = '{$employee_id}', description='{$desc}', updated_at = '{$date}' WHERE id = '{$id}' AND parent_user_id = '{$userid}' ";
		$this->_obj->sql_query($sql);
	}

	function selectAllSubusers($userid)
	{
		$sql = "SELECT id, name, employee_id, description, parent_user_id, unique_token, status FROM user WHERE parent_user_id = '{$userid}' ";
		$return = $this->_obj->select($sql);
		return $return;
	}

	/**
	 * @desc   check sub user is valid
	 */

	function selectSubUser($id, $user_id)
	{
		$sql = "SELECT * FROM user WHERE id = '{$id}' AND parent_user_id = '{$user_id}' AND status = '1'";
		$return = $this->_obj->select($sql);
		return $return;
	}

	/**
	 * @desc delete sub user
	 */

	function deleteSubUser($id, $user_id, $date)
	{
		$sql = "UPDATE user SET status = '2', updated_at = '{$date}' WHERE id = '{$id}' AND parent_user_id = '{$user_id}'";
		$this->_obj->sql_query($sql);
		return;
	}

	/**
	 * @desc delete sub user
	 */

	function resetSubUser($id, $user_id, $password, $date)
	{
		$sql = "UPDATE user SET password = '{$password}', updated_at = '{$date}' WHERE id = '{$id}' AND parent_user_id = '{$user_id}'";
		$this->_obj->sql_query($sql);
		return;
	}

	/**
	 * @desc login sub user
	 */

	function get_subUserLogin($password = null, $email = null)
	{
		$sql = "SELECT u.* FROM user as u LEFT JOIN user as u1 ON u1.id = u.parent_user_id WHERE (u.password ='{$password}' AND u.employee_id='{$email}') AND u.status = '1' AND u1.status != '2' AND u1.id != 0";
		$row = $this->_obj->select($sql);
		return $row;

	}

}