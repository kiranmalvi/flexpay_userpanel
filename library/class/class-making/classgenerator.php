<?php
include("resources/class.database.php");
$database = new Database();

if ($_REQUEST["f"] == "") {
	?>

	<font face="Arial" size="3"><b>
			PHP MYSQL Class Generator
		</b>
	</font>

	<font face="Arial" size="2"><b>

			<form action="classgenerator.php" method="POST" name="FORMGEN">

				1) Select Table Name:
				<br>

				<select name="tablename" id="tablename" onchange="autoclassname();">

					<?php
					$database->OpenLink();
					$dbname = $database->database;
					$tablelist = mysql_list_tables($dbname, $database->link);
					while ($row = mysql_fetch_row($tablelist)) {
						$sql = "SELECT COLUMN_NAME  FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . $dbname . "' AND TABLE_NAME = '" . $row[0] . "'  AND COLUMN_KEY = 'PRI'";
//echo $sql = "SELECT k.column_name FROM information_schema.table_constraints t JOIN information_schema.key_column_usage k USING(constraint_name,table_schema,table_name) WHERE t.constraint_type='PRIMARY KEY'   AND t.table_schema='".$dbname."'   AND t.table_name='".$row[0]."'";
						$primary = mysql_fetch_assoc(mysql_query($sql));
						print "<option value=\"$row[0]\" data-primary=\"$primary[COLUMN_NAME]\" id=\"$row[0]\">$row[0]</option>";
					}
					?>
				</select>

				<p>
					2) Type Class Name (ex. "test"): <br>
					<input type="text" name="classname" size="50" value="" id="classname" readonly>

				<p>
					3) Type Name of Key Field:
					<br>
					<input type="text" name="keyname" value="" size="50" id="primarykey" readonly>
					<br>
					<font size=1>
						(Name of key-field with type number with autoincrement!)
					</font>

				<p>
					<script src="jquery-2.1.1.js"></script>
					<script>
						function autoclassname() {
							var selected_table = $('#tablename').val();
							$('#classname').val(selected_table);
							$('#primarykey').val(document.getElementById(selected_table).getAttribute("data-primary"));
						}
					</script>

					<input type="submit" name="s1" value="Generate Class">
					<input type="hidden" name="f" value="formshowed">

			</form>

		</b>
	</font>
	<p>
		<font size="1" face="Arial">
			<a href="http://www.voegeli.li" target="_blank">
				this is a script from www.voegeli.li
			</a>
		</font>
		<script>
			$(document).ready(function () {
				autoclassname();
			});
		</script>

	<?php
} else {

// fill parameters from form
	$table = $_REQUEST["tablename"];
	$class = $_REQUEST["classname"];
	$key = $_REQUEST["keyname"];

	$dir = dirname(__FILE__);

	$filename = $dir . "/generated_classes/" . $class . ".class.php";

// if file exists, then delete it
	if (file_exists($filename)) {
		unlink($filename);
	}

// open file in insert mode
	$file = fopen($filename, "w+");
	$filedate = date("d.m.Y");

	$c = "<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    $class
* DATE:         $filedate
* CLASS FILE:   $filename
* TABLE:        $table
* DB:           $database->database
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class $class
{


/**
*   @desc Variable Declaration with default value
*/

\tprotected $$key;   // KEY ATTR. WITH AUTOINCREMENT
";

	$sql = "SHOW COLUMNS FROM $table;";
	$database->query($sql);
	$result = $database->result;


	while ($row = mysql_fetch_row($result)) {
		$col = "_" . $row[0];

		if ($col != $key) {

			$c .= "
\tprotected $$col;  ";


		} // endif
//"print "$col";
	} // endwhile

	$cdb = "$" . "database";
	$cdb2 = "database";
	$c .= "

";

	$cthis = "$" . "this->_";
	$thisdb = "global $" . "obj;\n\t";
	$thisdb .= "\t" . $cthis . "obj = $" . "obj;";

	$unsetthisdb = "unset(" . $cthis . "obj);";

	$sql = "SHOW COLUMNS FROM $table;";
	$database->query($sql);
	$result = $database->result;

	$c .= "

/**
*   @desc   CONSTRUCTOR METHOD
*/

\tfunction __construct()
\t{
	\t$thisdb
";
	while ($row = mysql_fetch_row($result)) {
		$col = $row[0];
		$colthis = "\n\t\t$" . "this->_" . $col . " = null;";
		$c .= "$colthis ";
	}
	$c .= "
\t}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

\tfunction __destruct()
\t{
	\t$unsetthisdb
\t}

";

	$c .= "

/**
*   @desc   GETTER METHODS
*/

";


// GETTER
	$database->query($sql);
	$result = $database->result;
	while ($row = mysql_fetch_row($result)) {
		$col = $row[0];
		$mname = "get" . $col . "()";
		$mthis = "$" . "this->_" . $col;
		$c .= "
\tpublic function $mname
\t{
\t\treturn $mthis;
\t}
";
	}


	$c .= "

/**
*   @desc   SETTER METHODS
*/

";
// SETTER
	$database->query($sql);
	$result = $database->result;
	while ($row = mysql_fetch_row($result)) {
		$col = $row[0];
		$val = "$" . "val";
		$mname = "set" . $col . "($" . "val)";
		$mthis = "$" . "this->_" . $col . " = ";
		$c .= "
\tpublic function $mname
\t{
\t\t $mthis $val;
\t}
";
	}


	$sql = "$" . "sql = ";
	$id = "$" . "id";
	$thisdb = "$" . "this->" . "obj";
	$thisdbquery = "$" . "this->" . "_obj->select($" . "sql" . ")";
	$result = "$" . "row = ";
	$row = "$" . "row";
	$result1 = "$" . "result";
//$res = "$" . "result = $" . "this->obj->result;";

	$c .= "

/**
*   @desc   SELECT METHOD / LOAD
*/

\tfunction select($id=null)
\t{";
	$c .= "\n\t\t $" . "WHERE = 'WHERE 1=1';";
	$c .= "\n\t\t if(!empty(" . "$" . "id)) {";
	$c .= "\n\t\t\t $" . "WHERE .= ' AND $key = '." . $id . ";";
	$c .= "\n\t\t }";
	$c .= "
\t\t $sql \"SELECT * FROM $table " . "$" . "WHERE\";
\t\t $result $thisdbquery; \n
";
	$sql = "SHOW COLUMNS FROM $table;";
	$database->query($sql);
	$result = $database->result;
	while ($row = mysql_fetch_row($result)) {
		$col = $row[0];
//$cthis = "$" . "this->" . $col . " = $" . "row->" . $col;
		$cthis = " $" . "this->_" . $col . " = $" . "row[0]['" . $col . "']";

		$c .= "\t\t$cthis;
";
	}
	$c .= "\t\t return" . ' $' . 'row;';
	$c .= "\n\t}";

	$zeile1 = "$" . "sql" . " = \"DELETE FROM $table WHERE $key = $id\"";
	$zeile2 = "$" . "this->_obj->sql_query($" . "sql);";
	$c .= "


/**
*   @desc   DELETE
*/

\tfunction delete($id)
\t{
\t\t $zeile1;
\t\t $zeile2
";
	$c .= "\t}
";


	$zeile1 = "$" . "this->$key = \"\"";
	$zeile2 = "INSERT INTO $table (";
	$zeile5 = ")";
	$zeile3 = "";
	$zeile4 = "";
	$zeile6 = "VALUES (";

	$sql = "SHOW COLUMNS FROM $table;";
	$database->query($sql);
	$result = $database->result;
	while ($row = mysql_fetch_row($result)) {
		$col = $row[0];

		if ($col != $key) {
			$zeile3 .= "$col" . ",";
//$zeile4.= "'$" . "this->_$col" . "',";
			$zeile4 .= "'\".$" . "this->_$col" . ".\"',";
//$zeile3 = rtrim($zeile3);
//$zeile4 = rtrim($zeile4);
//$zeile3 = str_replace(",", " ", $zeile3);
//$zeile4 = str_replace(",", " ", $zeile4);


		}

	}

	$zeile3 = substr($zeile3, 0, -1);
	$zeile4 = substr($zeile4, 0, -1);
	$sql = "$" . "sql =";
	$zeile7 = "$" . "result = $" . "this->_obj->insert($" . "sql);";
	$zeile8 = "$" . "row";
	$zeile9 = "$" . "result";
	$zeile10 = "return " . "$" . "result;";
//$zeile10 = "$" . "this->$key = " . "mysql_insert_id($" . "this->database->link);";

	$c .= "

/**
*   @desc   INSERT
*/

\tfunction insert()
\t{
\t\t $zeile1; // clear key for autoincrement

\t\t $sql \"$zeile2 $zeile3 $zeile5 $zeile6 $zeile4 $zeile5\";
\t\t $zeile7
\t\t $zeile10
\t}
";


// UPDATE ----------------------------------------

	$zeile1 = "$" . "this->_$key = \"\"";
	$zeile2 = "UPDATE $table SET ";
	$zeile5 = ")";
	$zeile3 = "";
	$zeile4 = "";
	$zeile6 = "VALUES (";

	$upd = "";

	$sql = "SHOW COLUMNS FROM $table;";
	$database->query($sql);
	$result = $database->result;
	while ($row = mysql_fetch_row($result)) {
		$col = $row[0];

		if ($col != $key) {
			$zeile3 .= "$col" . ",";
			$zeile4 .= "$" . "this->_$col" . ",";
			$upd .= "" . "$col = '\".$" . "this->_$col.\"' , ";
//$zeile3 = rtrim($zeile3);
//$zeile4 = rtrim($zeile4);
//$zeile3 = str_replace(",", " ", $zeile3);
//$zeile4 = str_replace(",", " ", $zeile4);


		}

	}

	$zeile3 = substr($zeile3, 0, -1);
	$zeile4 = substr($zeile4, 0, -2);
	$upd = substr($upd, 0, -2);
	$sql = "$" . "sql = \"";
	$zeile7 = "$" . "this->_obj->sql_query($" . "sql)";
	$zeile8 = "$" . "row";
	$zeile9 = "$" . "result";
	$zeile10 = "$" . "this->_$key = $" . "row->$key";
	$id = "$" . "id";
	$where = "WHERE " . "$key = $" . "id";

	$c .= "

/**
*   @desc   UPDATE
*/

\tfunction update($id)
\t{

\t\t $sql $zeile2 $upd $where \";
\t\t $zeile7;
";
	$c .= "
\t}
";

	$c .= "

}
";
	fwrite($file, $c);

	print "
<font face=\"Arial\" size=\"3\"><b>
PHP MYSQL Class Generator
</b>
<p>
<font face=\"Arial\" size=\"2\"><b>
Class '$class' successfully generated as file '$filename'!
<p>
<a href=\"javascript:history.back();\">
back
</a>

</b></font>

";

} // endif