<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    transaction
 * DATE:         28.10.2015
 * CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/transaction.class.php
 * TABLE:        transaction
 * DB:           flexpay
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class transaction
{


	/**
	 * @desc Variable Declaration with default value
	 */

	protected $transaction_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_transaction_id;
	protected $_transaction_unique_id;
	protected $_from_user_id;
	protected $_to_user_id;
	protected $_amount;
	protected $_type;
	protected $_comment;
	protected $_transaction_status;
	protected $_added_at;
	protected $_updated_at;
	protected $_status;


	/**
	 * @desc   CONSTRUCTOR METHOD
	 */

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_transaction_id = null;
		$this->_transaction_unique_id = null;
		$this->_from_user_id = null;
		$this->_to_user_id = null;
		$this->_amount = null;
		$this->_type = null;
		$this->_comment = null;
		$this->_transaction_status = null;
		$this->_added_at = null;
		$this->_updated_at = null;
		$this->_status = null;
	}

	/**
	 * @desc   DECONSTRUCTOR METHOD
	 */

	function __destruct()
	{
		unset($this->_obj);
	}


	/**
	 * @desc   GETTER METHODS
	 */


	public function gettransaction_id()
	{
		return $this->_transaction_id;
	}

	public function gettransaction_unique_id()
	{
		return $this->_transaction_unique_id;
	}

	public function getfrom_user_id()
	{
		return $this->_from_user_id;
	}

	public function getto_user_id()
	{
		return $this->_to_user_id;
	}

	public function getamount()
	{
		return $this->_amount;
	}

	public function gettype()
	{
		return $this->_type;
	}

	public function getcomment()
	{
		return $this->_comment;
	}

	public function gettransaction_status()
	{
		return $this->_transaction_status;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}

	public function getstatus()
	{
		return $this->_status;
	}


	/**
	 * @desc   SETTER METHODS
	 */


	public function settransaction_id($val)
	{
		$this->_transaction_id = $val;
	}

	public function settransaction_unique_id($val)
	{
		$this->_transaction_unique_id = $val;
	}

	public function setfrom_user_id($val)
	{
		$this->_from_user_id = $val;
	}

	public function setto_user_id($val)
	{
		$this->_to_user_id = $val;
	}

	public function setamount($val)
	{
		$this->_amount = $val;
	}

	public function settype($val)
	{
		$this->_type = $val;
	}

	public function setcomment($val)
	{
		$this->_comment = $val;
	}

	public function settransaction_status($val)
	{
		$this->_transaction_status = $val;
	}

	public function setadded_at($val)
	{
		$this->_added_at = $val;
	}

	public function setupdated_at($val)
	{
		$this->_updated_at = $val;
	}

	public function setstatus($val)
	{
		$this->_status = $val;
	}


	/**
	 * @desc   SELECT METHOD / LOAD
	 */

	function select($id = null)
	{
		$WHERE = 'WHERE 1=1';
		if (!empty($id)) {
			$WHERE .= ' AND transaction_id = ' . $id;
		}
		$sql = "SELECT * FROM transaction $WHERE";
		$row = $this->_obj->select($sql);

		$this->_transaction_id = $row[0]['transaction_id'];
		$this->_transaction_unique_id = $row[0]['transaction_unique_id'];
		$this->_from_user_id = $row[0]['from_user_id'];
		$this->_to_user_id = $row[0]['to_user_id'];
		$this->_amount = $row[0]['amount'];
		$this->_type = $row[0]['type'];
		$this->_comment = $row[0]['comment'];
		$this->_transaction_status = $row[0]['transaction_status'];
		$this->_added_at = $row[0]['added_at'];
		$this->_updated_at = $row[0]['updated_at'];
		$this->_status = $row[0]['status'];
		return $row;
	}


	/**
	 * @desc   DELETE
	 */

	function delete($id)
	{
		$sql = "DELETE FROM transaction WHERE transaction_id = $id";
		$this->_obj->sql_query($sql);
	}


	/**
	 * @desc   INSERT
	 */

	function insert()
	{
		$this->transaction_id = ""; // clear key for autoincrement

		$sql = "INSERT INTO transaction ( transaction_unique_id,from_user_id,to_user_id,amount,type,comment,transaction_status,added_at,updated_at,status ) VALUES ( '" . $this->_transaction_unique_id . "','" . $this->_from_user_id . "','" . $this->_to_user_id . "','" . $this->_amount . "','" . $this->_type . "','" . $this->_comment . "','" . $this->_transaction_status . "','" . $this->_added_at . "','" . $this->_updated_at . "','" . $this->_status . "' )";
		$result = $this->_obj->insert($sql);
		return $result;
	}


	/**
	 * @desc   UPDATE
	 */

	function update($id)
	{

		$sql = " UPDATE transaction SET  transaction_unique_id = '" . $this->_transaction_unique_id . "' , from_user_id = '" . $this->_from_user_id . "' , to_user_id = '" . $this->_to_user_id . "' , amount = '" . $this->_amount . "' , type = '" . $this->_type . "' , comment = '" . $this->_comment . "' , transaction_status = '" . $this->_transaction_status . "' , added_at = '" . $this->_added_at . "' , updated_at = '" . $this->_updated_at . "' , status = '" . $this->_status . "'  WHERE transaction_id = $id ";
		$this->_obj->sql_query($sql);

	}


}
