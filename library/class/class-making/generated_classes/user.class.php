<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    user
 * DATE:         28.10.2015
 * CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/user.class.php
 * TABLE:        user
 * DB:           flexpay
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class user
{


	/**
	 * @desc Variable Declaration with default value
	 */

	protected $id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_id;
	protected $_name;
	protected $_email;
	protected $_password;
	protected $_mobile;
	protected $_security_pin;
	protected $_company_name;
	protected $_added_at;
	protected $_updated_at;
	protected $_status;


	/**
	 * @desc   CONSTRUCTOR METHOD
	 */

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_id = null;
		$this->_name = null;
		$this->_email = null;
		$this->_password = null;
		$this->_mobile = null;
		$this->_security_pin = null;
		$this->_company_name = null;
		$this->_added_at = null;
		$this->_updated_at = null;
		$this->_status = null;
	}

	/**
	 * @desc   DECONSTRUCTOR METHOD
	 */

	function __destruct()
	{
		unset($this->_obj);
	}


	/**
	 * @desc   GETTER METHODS
	 */


	public function getid()
	{
		return $this->_id;
	}

	public function getname()
	{
		return $this->_name;
	}

	public function getemail()
	{
		return $this->_email;
	}

	public function getpassword()
	{
		return $this->_password;
	}

	public function getmobile()
	{
		return $this->_mobile;
	}

	public function getsecurity_pin()
	{
		return $this->_security_pin;
	}

	public function getcompany_name()
	{
		return $this->_company_name;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}

	public function getstatus()
	{
		return $this->_status;
	}


	/**
	 * @desc   SETTER METHODS
	 */


	public function setid($val)
	{
		$this->_id = $val;
	}

	public function setname($val)
	{
		$this->_name = $val;
	}

	public function setemail($val)
	{
		$this->_email = $val;
	}

	public function setpassword($val)
	{
		$this->_password = $val;
	}

	public function setmobile($val)
	{
		$this->_mobile = $val;
	}

	public function setsecurity_pin($val)
	{
		$this->_security_pin = $val;
	}

	public function setcompany_name($val)
	{
		$this->_company_name = $val;
	}

	public function setadded_at($val)
	{
		$this->_added_at = $val;
	}

	public function setupdated_at($val)
	{
		$this->_updated_at = $val;
	}

	public function setstatus($val)
	{
		$this->_status = $val;
	}


	/**
	 * @desc   SELECT METHOD / LOAD
	 */

	function select($id = null)
	{
		$WHERE = 'WHERE 1=1';
		if (!empty($id)) {
			$WHERE .= ' AND id = ' . $id;
		}
		$sql = "SELECT * FROM user $WHERE";
		$row = $this->_obj->select($sql);

		$this->_id = $row[0]['id'];
		$this->_name = $row[0]['name'];
		$this->_email = $row[0]['email'];
		$this->_password = $row[0]['password'];
		$this->_mobile = $row[0]['mobile'];
		$this->_security_pin = $row[0]['security_pin'];
		$this->_company_name = $row[0]['company_name'];
		$this->_added_at = $row[0]['added_at'];
		$this->_updated_at = $row[0]['updated_at'];
		$this->_status = $row[0]['status'];
		return $row;
	}


	/**
	 * @desc   DELETE
	 */

	function delete($id)
	{
		$sql = "DELETE FROM user WHERE id = $id";
		$this->_obj->sql_query($sql);
	}


	/**
	 * @desc   INSERT
	 */

	function insert()
	{
		$this->id = ""; // clear key for autoincrement

		$sql = "INSERT INTO user ( name,email,password,mobile,security_pin,company_name,added_at,updated_at,status ) VALUES ( '" . $this->_name . "','" . $this->_email . "','" . $this->_password . "','" . $this->_mobile . "','" . $this->_security_pin . "','" . $this->_company_name . "','" . $this->_added_at . "','" . $this->_updated_at . "','" . $this->_status . "' )";
		$result = $this->_obj->insert($sql);
		return $result;
	}


	/**
	 * @desc   UPDATE
	 */

	function update($id)
	{

		$sql = " UPDATE user SET  name = '" . $this->_name . "' , email = '" . $this->_email . "' , password = '" . $this->_password . "' , mobile = '" . $this->_mobile . "' , security_pin = '" . $this->_security_pin . "' , company_name = '" . $this->_company_name . "' , added_at = '" . $this->_added_at . "' , updated_at = '" . $this->_updated_at . "' , status = '" . $this->_status . "'  WHERE id = $id ";
		$this->_obj->sql_query($sql);

	}


}
