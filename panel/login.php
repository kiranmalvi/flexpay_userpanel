<?php
require_once('include.php');
if (!empty($_SESSION[$Project_Name_Members])) {
	header('location:index.php?file=a-dashboard');
} else {
	$email = "";
	$pass = "";
	if (isset($_REQUEST['members_login'])) {
		$email = $_REQUEST['email'];
		$pass = $_REQUEST['password'];
		$password = md5($pass);
		$user = new user();
		$error = $user->members_do_login($email, $password);
		header("Location:login.php");
		exit;
	}

	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Flex Pay</title>
		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="<?php echo $site_url; ?>css/bootstrap_vnew.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $site_url; ?>css/style_vnew.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $site_url; ?>css/style_vnew.css">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<style>

			body {
				width: 100%;
				background: #000000 url(../img/bannerimage.png) !important;
				background-position: right top !important;
				background-size: cover !important;
				padding-bottom: 26px !important;
			}

		</style>

	</head>
	<body class="loginbg-dollor">

	<section class="hadertop-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a class="logoimage"><img src="<?php echo $site_url; ?>img/logo.png" alt=""/></a>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-md-offset-1">
					<h1 class="loginbg-dollor-text">Login</h1>
					<div class="loginbg-dollor-discrition">Flex Pay offers you the ability to top up your smart phone
						and use it at selected vendors just like cash, no ID required, just scan the QR code and go.
						"Don't have an Flex Pay account? download our app first and sign up."
					</div>
					<form method="post" action="#" id="member_login_form">
						<div class="logo-form">
							<input type="text" name="email" id="email" placeholder="Email Address">
						</div>
						<div class="logo-form">
							<input type="password" name="password" id="password" placeholder="Password">
						</div>
						<input type="submit" value="Submit" name="members_login" id="members_login" class="logo-submit">
					</form>
				</div>
			</div>
		</div>
	</section>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<?php echo $site_url; ?>js/bootstrap_vnew.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $members_url ?>assets/skin/default_skin/css/theme.css">
	<script src="<?php echo $members_url ?>assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo $members_url ?>assets/sweetalert2/dist/sweetalert2.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $members_url ?>assets/sweetalert2/dist/sweetalert2.css">
	<script>
		var FormAdminValidator = function () {
			// function to initiate Validation Sample 1
			var temp = 0;
			var runValidator1 = function () {
				var form1 = $('#member_login_form');

				$('#member_login_form').validate({

					errorElement: "span", // contain the error msg in a span tag
					errorClass: 'help-block',
					errorPlacement: function (error, element) { // render error placement for each input type
						error.insertAfter(element);
						// for other inputs, just perform default behavior
					},
					ignore: "",
					rules: {
						email: {
							email: true,
							required: true
						},
						password: {
							required: true
						}

					},
					messages: {
						email: {
							required: "Please enter email address",
							email: "Please enter valid email address"
						},
						password: "Please enter password"
					},
					invalidHandler: function (event, validator) { //display error alert on form submit
						//successHandler1.hide();
						//errorHandler1.show();
					},
					highlight: function (element) {
						$(element).closest('.help-block').removeClass('valid');
						// display OK icon
						$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
						// add the Bootstrap error class to the control group
					},
					unhighlight: function (element) { // revert the change done by hightlight
						$(element).closest('.form-group').removeClass('has-error');
						// set error class to the control group
					},
					success: function (label, element) {
						label.addClass('help-block valid');
						// mark the current input as valid and display OK icon
						$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
					},
					submitHandler: function (frmadd) {
						successHandler1.show();
						errorHandler1.hide();
					}

				});

			};

			return {
				//main function to initiate template pages
				init: function () {
					runValidator1();
				}
			};

			//$('#frmadd').submit();
		}();

		//$('#frmadd').submit();
		$(document).ready(function () {
			FormAdminValidator.init();
			$('#members_login').click(function () {
				$('#member_login_form').submit();
			});
		});
	</script>
	<script>
		$(document).ready(function () {
			<?php
			if(isset($_SESSION['tmp_message']) && $_SESSION['tmp_message'] == 'success'){
			?>
			swal(
				{
					html: "You are not authorized to access this panel. Please contact your administrator."
				});
			<?php
			unset($_SESSION['tmp_message']);
			}
			?>

			<?php
			if(isset($_SESSION['tmp_message1']) && $_SESSION['tmp_message1'] == 'inactive'){
			?>
			swal(
				{
					html: "This account has been inactivate."
				});
			<?php
			unset($_SESSION['tmp_message1']);
			}
			?>

			<?php
			if(isset($_SESSION['tmp_message2']) && $_SESSION['tmp_message2'] == 'deleted'){
			?>
			swal(
				{
					html: "This account has been deleted."
				});
			<?php
			unset($_SESSION['tmp_message2']);
			}
			?>

			<?php
			if(isset($_SESSION['tmp_message3']) && $_SESSION['tmp_message3'] == 'block'){
			?>
			swal(
				{
					html: "Your account has been blocked contact admin for further information."
				});
			<?php
			unset($_SESSION['tmp_message3']);
			}
			?>

			<?php
			if(isset($_SESSION['tmp_message4']) && $_SESSION['tmp_message4'] == 'unverified'){
			?>
			swal(
				{
					html: "This account has been unverified."
				});
			<?php
			unset($_SESSION['tmp_message4']);
			}
			?>
		});
	</script>
	</body>
	</html>
<?php } ?>