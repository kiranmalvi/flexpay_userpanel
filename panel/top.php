<!-- Start: Header -->
<header class="navbar navbar-fixed-top bg-system">
	<div class="navbar-branding bg-system">
		<a class="navbar-brand" href="index.php?file=a-dashboard"> <b><?php echo $ADMIN_PANEL_TITLE; ?></b></a>
		<span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
		<ul class="nav navbar-nav pull-right hidden">
			<li>
				<a href="#" class="sidebar-menu-toggle">
					<span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
				</a>
			</li>
		</ul>
	</div>

	<ul class="nav navbar-nav navbar-right">

		<li class="dropdown">
			<a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown">
				<span><?php echo $_SESSION[$Project_Name_Members]['MEMBERS']['user_name']; ?></span>
				<span class="caret caret-tp"></span>
			</a>
			<ul class="dropdown-menu dropdown-persist pn w250 bg-white" role="menu">
				<li class="br-t of-h">
					<a href="<?php echo "index.php?file=a-changepassword" ?>"
					   class="fw600 p12 animated animated-short fadeInDown">
						<span class="glyphicons glyphicons-fire "></span> Change Password </a>
				</li>
				<li class="br-t of-h">
					<a href="<?php echo $members_url . "logout.php" ?>"
					   class="fw600 p12 animated animated-short fadeInDown">
						<span class="fa fa-power-off pr5"></span> Logout </a>
				</li>
			</ul>
		</li>
	</ul>
</header>
<!-- End: Header -->