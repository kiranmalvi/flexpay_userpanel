<!DOCTYPE html>
<html>


<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title><?php echo $ADMIN_PAGE_TITLE; ?></title>
	<meta name="keywords" content=""/>
	<meta name="description" content="">
	<meta name="author" content="Mindinventory">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Summernote CSS  -->

	<!-- Theme CSS -->


	<!-- Favicon -->


	<!-- Font CSS (Via CDN) --><!-- Font CSS (Via CDN) -->

	<link rel="stylesheet" href="assets/animation/animsition.min.css">

	<link rel='stylesheet' type='text/css' href='assets/CSS/family_Open+Sans.css'>
	<link rel='stylesheet' type='text/css' href='assets/CSS/family=Roboto.css'>

	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

	<link rel="stylesheet" type="text/css"
		  href="<?php echo $members_url ?>vendor/plugins/datatables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css"
		  href="<?php echo $members_assets_url ?>js/utility/highlight/styles/googlecode.css">
	<link rel="stylesheet" type="text/css"
		  href="<?php echo $members_assets_url ?>admin-tools/admin-forms/css/admin-forms.css">
	]
	<!-- start: CSS REQUIRED FOR MODAL POPUP ONLY -->
	<link href="<?php echo $members_assets_url ?>plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
		  rel="stylesheet"
		  type="text/css"/>
	<link href="<?php echo $members_assets_url ?>plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
		  type="text/css"/>
	<!-- end: CSS REQUIRED FOR MODAL POPUP ONLY -->

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo $members_url ?>img/flexpay-favicon.png" type="image/x-icon">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
		@media print {
			.noprint {
				display: none;
			}
		}
	</style>
</head>