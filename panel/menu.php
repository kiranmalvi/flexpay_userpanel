<!-- Start: Sidebar -->
<aside id="sidebar_left" class="ano nano-primary has-scrollbar sidebar-default affix">
	<div class="nano-content">
		<?php
		$iId = @$_REQUEST['iId'];
		# echo $script;
		?>

		<!-- sidebar menu -->
		<ul class="nav sidebar-menu">

			<li <?php echo (in_array($script, array('dashboard'))) ? 'class="active"' : ''; ?>>
				<a href="index.php" class="animsition-link">
					<span class="glyphicons glyphicons-coins"></span>
					<span class="sidebar-title">My Balance</span>
				</a>
			</li>

			<li <?php echo (in_array($script, array('sendmoney_list'))) ? 'class="active"' : ''; ?>>
				<a href="index.php?file=sm-sendmoney_list" class="animsition-link">
					<span class="glyphicons glyphicons-left_arrow"></span>
					<span class="sidebar-title">Send Money</span>
				</a>
			</li>

			<li <?php echo (in_array($script, array('receivemoney_list'))) ? 'class="active"' : ''; ?>>
				<a href="index.php?file=rm-receivemoney_list" class="animsition-link">
					<span class="glyphicons glyphicons-right_arrow"></span>
					<span class="sidebar-title">Receive Money</span>
				</a>
			</li>
		</ul>
		<!--        <div class="sidebar-toggle-mini">-->
		<!--            <a href="#">-->
		<!--                <span class="fa fa-sign-out"></span>-->
		<!--            </a>-->
		<!--        </div>-->
	</div>
</aside>
<!-- End: Sidebar -->