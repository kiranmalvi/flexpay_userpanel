<?php
require_once('include.php');
$password = "";
if (isset($_REQUEST['unlock'])) {
	$email = $_SESSION[$Project_Name_Members]['MEMBERS']['email'];
	$password = md5($_REQUEST['password']);

	$admin = new admin();
	$error = $admin->do_login($email, $password);
}

session_cache_limiter('private');
$cache_limiter = session_cache_limiter();

/* set the cache expire to 30 minutes */
session_cache_expire(20);
$cache_expire = session_cache_expire();

/* start the session */

session_start();

//echo "The cache limiter is now set to $cache_limiter<br />";
$session_msg = "You are lock for ";

?>

<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title><?php echo $ADMIN_PAGE_TITLE; ?></title>
	<meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme"/>
	<meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
	<meta name="author" content="AdminDesigns">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Font CSS (Via CDN) -->
	<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,300">

	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo $site_url ?>skin/default_skin/css/theme.css">

	<!-- Admin Forms CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo $site_url ?>admin-tools/admin-forms/css/admin-forms.css">

	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.ico">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>

<body class="external-page sb-l-c sb-r-c">

<!-- Start: Main -->
<div id="main" class="animated fadeIn">

	<!-- Start: Content -->
	<section id="content_wrapper">

		<!-- begin canvas animation bg -->
		<div id="canvas-wrapper">
			<canvas id="demo-canvas"></canvas>
		</div>

		<!-- Begin: Content -->
		<section id="content">

			<div class="admin-form theme-info mw600" style="margin-top: 14%;" id="login1">
				<div class="row mb15 table-layout">

					<div class="col-xs-6 va-m pln">
						<a href="javascript:" title="pigi7" style="text-decoration: none;">
							<h1><i><span style="color: #ffffff;"><?php echo $ADMIN_PANEL_TITLE; ?></span></i></h1>
						</a>
					</div>

					<div class="col-xs-6 text-right va-b pr5">
						<div class="login-links">
							<a href="<?php echo $members_url . "logout.php" ?>" class=""
							   title="False Credentials">Not <?php echo $_SESSION[$Project_Name_Members]['MEMBERS']['user_name']; ?>
								?</a>
						</div>

					</div>

				</div>
				<div class="panel panel-success mv10 heading-border br-n">

					<div class="panel-heading pn hidden">
						<span class="panel-title hidden"><i class="fa fa-sign-in"></i>Register</span>
					</div>

					<!-- end .form-header section -->
					<form method="post">

						<?php if (isset($error)) {
							echo '<br>';
							echo '<div class="alert alert-sm alert-border-left alert-danger light alert-dismissable">
                            <i class="fa fa-remove pr10"></i>
                            <strong>Sorry!</strong> ' . $error . '</div>';
						}
						?>

						<div class="panel-body bg-light pn">

							<div class="row table-layout">
								<div class="col-xs-9 p20 pv15 va-m bg-light">

									<h3 class="mb5"><?php echo $_SESSION[$Project_Name_Members]['MEMBERS']['user_name']; ?>
										<small> - <?php echo $session_msg; ?>
											<b> <?php echo $cache_expire . "Minutes"; ?>  </b>
									</h3>
									<p class="text-muted"><?php echo $_SESSION[$Project_Name_Members]['MEMBERS']['email'] ?></p>
									<input type="hidden" name="email"
										   value="<?php echo $_SESSION[$Project_Name_Members]['MEMBERS']['email'] ?>"/>

									<div class="section mt25">
										<label for="password" class="field prepend-icon">
											<input type="password" name="password" id="password"
												   value="<?php echo $password; ?>" class="gui-input"
												   placeholder="Enter password">
											<label for="password" class="field-icon"><i class="fa fa-lock"></i>
											</label>
										</label>
									</div>
									<!-- end section -->

								</div>
							</div>
						</div>

				</div>
				<div class="pull-right">
					<input type="submit" name="unlock" value="Unlock" class="button pull-right h-35">
				</div>
				</form>
			</div>

		</section>
		<!-- End: Content -->

	</section>
	<!-- End: Content-Wrapper -->

</div>
<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->

<!-- Google Map API -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>

<!-- jQuery -->
<script type="text/javascript" src="<?php echo $members_url ?>vendor/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo $members_url ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Bootstrap -->
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/bootstrap/bootstrap.min.js"></script>

<!-- Page Plugins -->
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/pages/login/EasePack.min.js"></script>
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/pages/login/rAF.js"></script>
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/pages/login/TweenLite.min.js"></script>
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/pages/login/login.js"></script>

<!-- Theme Javascript -->
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/utility/utility.js"></script>
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/main.js"></script>
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/demo.js"></script>

<!-- Page Javascript -->
<script type="text/javascript">
	jQuery(document).ready(function () {
		"use strict";
		// Init Theme Core
		Core.init();
		// Init Demo JS
		Demo.init();
		// Init CanvasBG and pass target starting location
		CanvasBG.init({
			Loc: {
				x: window.innerWidth / 2.1,
				y: window.innerHeight / 2.3
			}


		});
	});
</script>
<script>
	window.location.hash = "no-back-button";
	window.location.hash = "Again-No-back-button";//again because google chrome don't insert first hash into history
	window.onhashchange = function () {
		window.location.hash = "";
	}
</script
	<!-- END: PAGE SCRIPTS -->

</body>

</html>